import axios from '@plugins/axios';
import moment from 'moment';

export const state = {
    events: [],
    eventsQuantity: 0,
    createdAt: ''
};

export const getters = {
    events: state => state.events,
    eventsQuantity: state => state.eventsQuantity
};

export const mutations = {
    ADD_EVENTS(state, response) {
        const { results } = response;

        const filteredEvents = results.filter(isInProgress);

        state.events = filteredEvents;
    },

    EVENTS_QUANTITY(state, response) {
        const { count } = response;

        if (count) {
            state.eventsQuantity = count;
        }
    },

    CREATED_AT(state, datatime) {
        state.createdAt = datatime;
    }
};

export const actions = {
    async getEvents({ commit, state }) {
        const { events, createdAt, eventsQuantity } = state;

        if (eventsQuantity === 0 || timeAgo(createdAt) >= 300000) {
            const { data } = await axios.get(`/api/v1/events`);

            commit('ADD_EVENTS', data);
            commit('EVENTS_QUANTITY', data);
            commit('CREATED_AT', new Date().toISOString());

            return data;
        }

        return {
            count: eventsQuantity,
            results: events
        };
    }
};

function isInProgress(event) {
    const { date_start, date_end, status } = event;

    const now = moment(new Date().toISOString());
    const start = moment(date_start);
    const end = moment(date_end);

    return moment(now).isBetween(start, end) && status === 'active';
}

function timeAgo(created_at) {
    const now = moment(new Date().toISOString());
    const end = moment(created_at);
    const duration = moment.duration(now.diff(end));

    return duration.asMilliseconds();
}
