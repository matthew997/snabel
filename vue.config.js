const config = require('./src/config');

module.exports = {
    lintOnSave: true,
    publicPath: config.publicPath,
    filenameHashing: false,

    configureWebpack: {
        name: 'snabel-app',
        resolve: {
            alias: require('./aliases.config').webpack
        },

        output: {
            filename: '[name].bundle.js'
        },

        plugins: []
    },

    css: {
        sourceMap: false
    }
};
