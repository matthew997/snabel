import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    routes,
    base: '/',
    mode: 'history',
    linkActiveClass: 'active'
});

export default router;
