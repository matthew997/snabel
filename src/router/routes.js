export default [
    {
        path: '/',
        name: 'index',
        component: () => lazyLoadView(import('@views/Home')),
        meta: {
            layout: 'default',
            authRequired: false
        }
    },

    {
        path: '/cards',
        name: 'cards',
        component: () => lazyLoadView(import('@views/Cards')),
        meta: {
            layout: 'default',
            authRequired: false
        }
    },

    {
        path: '/*',
        name: 'error',
        component: () => lazyLoadView(import('@views/Error')),
        meta: {
            layout: 'error',
            authRequired: false
        }
    }
];

function lazyLoadView(AsyncView) {
    const AsyncHandler = () => ({
        component: AsyncView,
        delay: 400,
        timeout: 10000
    });

    return Promise.resolve({
        functional: true,
        render(h, { data, children }) {
            return h(AsyncHandler, data, children);
        }
    });
}
