require('dotenv').config();

module.exports = {
    apiBaseUrl: process.env.VUE_APP_API_BASE_URL,
    token: process.env.VUE_APP_TOKEN,
    publicPath: '/'
};
