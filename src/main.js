import Vue from 'vue';
import App from './App';
import router from './router';
import store from '@state/store';
import VueToast from 'vue-toast-notification';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueToast);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
