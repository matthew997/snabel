import axios from 'axios';
import config from '@config';

axios.defaults.baseURL = config.apiBaseUrl;
axios.defaults.headers.common['Authorization'] = config.token;

axios.interceptors.response.use(
    response => {
        return response;
    },

    error => {
        console.error(error);

        throw error;
    }
);

export default axios;
